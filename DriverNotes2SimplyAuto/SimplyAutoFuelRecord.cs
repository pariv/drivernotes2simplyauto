﻿using CsvHelper.Configuration.Attributes;

namespace DriverNotes2SimplyAuto
{
    internal class SimplyAutoFuelRecord
    {
        [Name("Row ID")]
        public int RowId { get; set; }

        [Name("Vehicle ID")]
        public string VehicleId { get; set; }

        [Name("Odometer")]
        public int Odometer { get; set; }

        [Name("Qty")]
        public float Qty { get; set; }

        [Name("Partial Tank")]
        public int PartialTank { get; set; }

        [Name("Missed Fill Up")]
        public int MissedFillUp { get; set; }

        [Name("Total Cost")]
        public float TotalCost { get; set; }

        [Name("Distance Travelled")]
        public int DistanceTravelled { get; set; }

        [Name("Eff")]
        public int Eff { get; set; }

        [Name("Octane")]
        public float Octane { get; set; }

        [Name("Fuel Brand")]
        public string FuelBrand { get; set; }

        [Name("Filling Station")]
        public string FillingStation { get; set; }

        [Name("Notes")]
        public string Notes { get; set; }

        [Name("Day")]
        public int Day { get; set; }

        [Name("Month")]
        public int Month { get; set; }

        [Name("Year")]
        public int Year { get; set; }

        [Name("Receipt Path")]
        public string ReceiptPath { get; set; }

        [Name("Latitude")]
        public float Latitude { get; set; }

        [Name("Longitude")]
        public float Longitude { get; set; }

        [Name("Record Type")]
        public int RecordType { get; set; }

        [Name("Record Desc")]
        public string RecordDesc { get; set; }


        public enum SimplyAutoRecordType
        {
            FuelRecord,
            ServiceRecord,
            OtherRecord
        }
    }
}
