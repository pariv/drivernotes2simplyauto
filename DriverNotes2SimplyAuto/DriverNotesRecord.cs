﻿using CsvHelper.Configuration.Attributes;
using System;

namespace DriverNotes2SimplyAuto
{
    internal class DriverNotesRecord
    {
        [Name("Дата")]
        public DateTime Date { get; set; }

        [Name("Пробег")]
        public int Odometer { get; set; }

        [Name("Тип события")]
        public DriverNotesRecordType RecordType { get; set; }

        [Name("Тип топлива(заправка)")]
        public string FuelType { get; set; }

        [Name("АЗС(заправка)")]
        public string FillingStation { get; set; }

        [Name("Объем(заправка)")]
        public float? FuelLiters { get; set; }

        [Name("Цена за литр(заправка)")]
        public float? FuelPricePerLiter { get; set; }

        [Name("Полный бак(заправка)")]
        [BooleanTrueValues("Да")]
        [BooleanFalseValues("Нет", "")]
        public bool FuelFullTank { get; set; }

        [Name("Пропустил заправку(заправка)")]
        [BooleanTrueValues("Да")]
        [BooleanFalseValues("Нет", "")]
        public bool MissedFillUp { get; set; }

        [Name("Название события(ремонт,тюнинг)")]
        public string ServiceEventName { get; set; }

        [Name("Узел(ремонт,тюнинг)")]
        public string ServiceNode { get; set; }

        [Name("Деталь(ремонт)")]
        public string ServiceDetail { get; set; }

        [Name("Название[производитель, модель](ремонт,тюнинг)")]
        public string ServiceDetailMakeModel { get; set; }

        [Name("Cтоимость детали(ремонт,тюнинг)")]
        public string ServiceDetailPrice { get; set; }

        [Name("Название СТО(ремонт,тюнинг)")]
        public string ServiceStationName { get; set; }

        [Name("Стоимость работы(ремонт,тюнинг)")]
        public string ServiceLabourPrice { get; set; }

        [BooleanTrueValues("Да")]
        [BooleanFalseValues("Нет", "")]
        [Name("Связано с ДТП(ремонт)")]
        public bool ServiceCausedByAccident { get; set; }

        [Name("Событие(другое)")]
        public string OtherEvent { get; set; }

        [Name("Место(другое)")]
        public string OtherLocation { get; set; }

        [Name("Общая стоимость")]
        public float? TotalCost { get; set; }

        [Name("Видимость")]
        [BooleanTrueValues("Да")]
        [BooleanFalseValues("Нет")]
        public bool Visibility { get; set; }

        [Name("Заметки")]
        public string Notes { get; set; }

        public enum DriverNotesRecordType
        {
            заправка,
            ремонт,
            тюнинг,
            другое
        }
    }
}
