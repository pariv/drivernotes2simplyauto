﻿using System;
using CsvHelper;
using CsvHelper.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace DriverNotes2SimplyAuto
{
    internal class Program
    {
        private static string _folderPath = @".\";
        private static readonly string[] ExceptFiles = { "Fuel_Log.csv", "Services.csv", "Vehicles.csv" };

        private static int Main(string[] args)
        {
            if (args.Length == 1)
            {
                if (!Directory.Exists(args[0]))
                {
                    Console.WriteLine($"Directory '{args[0]}' does not exist!");
                    return 1;
                }
                _folderPath = args[0];
            }
            else if (args.Length > 1)
            {
                Console.WriteLine($"No more then one argument permitted!");
                return 1;
            }
            Console.WriteLine($"Working directory: {Path.GetFullPath(_folderPath)}");

            var index = 0;
            var vehicles = Directory.GetFiles(_folderPath, "*.csv").Select(Path.GetFileName).Except(ExceptFiles).ToArray();
            if (vehicles.Length == 0)
            {
                Console.WriteLine("No logs found. Nothing to do.");
                return 0;
            }
            using (var writer = new StreamWriter(Path.Combine(_folderPath, "Fuel_Log.csv")))
            using (var vehiclesWriter = new StreamWriter(Path.Combine(_folderPath, "Vehicles.csv")))
            using (var csvWriter = new CsvWriter(writer, new Configuration(CultureInfo.GetCultureInfo("en-GB"))))
            using (var csvVehiclesWriter = new CsvWriter(vehiclesWriter, new Configuration(CultureInfo.GetCultureInfo("en-GB"))))
            {
                csvWriter.WriteHeader<SimplyAutoFuelRecord>();
                csvWriter.NextRecord();
                csvVehiclesWriter.WriteHeader<SimplyAutoVehicle>();
                csvVehiclesWriter.NextRecord();
                var vehicleIndex = 0;
                foreach (var vehicle in vehicles)
                {
                    vehicleIndex++;
                    var vehicleRecord = new SimplyAutoVehicle()
                    {
                        RowId = vehicleIndex,
                        VehicleId = Path.GetFileNameWithoutExtension(vehicle)?.Replace('_', ' '),
                        Make = Path.GetFileNameWithoutExtension(vehicle)?.Split('_')[0],
                        Model = Path.GetFileNameWithoutExtension(vehicle)?.Split('_')[1]
                    };
                    csvVehiclesWriter.WriteRecord(vehicleRecord);
                    csvVehiclesWriter.NextRecord();
                    using (var reader = new StreamReader(Path.Combine(_folderPath, vehicle), Encoding.GetEncoding("windows-1251")))
                    using (var csvReader = new CsvReader(reader, new Configuration(CultureInfo.GetCultureInfo("ru-RU")) { Delimiter = ";" }))
                    {
                        var vehicleId = Path.GetFileNameWithoutExtension(vehicle).Replace('_', ' ');
                        var records = csvReader.GetRecords<DriverNotesRecord>();

                        foreach (var driverNotesRecord in records)
                        {
                            index++;
                            var simplyAutoRecord = Convert(driverNotesRecord);
                            simplyAutoRecord.RowId = index;
                            simplyAutoRecord.VehicleId = vehicleId;
                            csvWriter.WriteRecord(simplyAutoRecord);
                            csvWriter.NextRecord();

                        }
                    }
                }
            }
            Console.WriteLine($"Successfully finished. {index} records converted.");
            return 0;
        }

        private static SimplyAutoFuelRecord Convert(DriverNotesRecord source)
        {
            var target = new SimplyAutoFuelRecord();
            switch (source.RecordType)
            {
                case DriverNotesRecord.DriverNotesRecordType.заправка:
                    target.RecordType = (int)SimplyAutoFuelRecord.SimplyAutoRecordType.FuelRecord;
                    break;
                case DriverNotesRecord.DriverNotesRecordType.ремонт:
                    target.RecordType = (int)SimplyAutoFuelRecord.SimplyAutoRecordType.ServiceRecord;
                    break;
                case DriverNotesRecord.DriverNotesRecordType.тюнинг:
                    target.RecordType = (int)SimplyAutoFuelRecord.SimplyAutoRecordType.ServiceRecord;
                    break;
                case DriverNotesRecord.DriverNotesRecordType.другое:
                    target.RecordType = (int)SimplyAutoFuelRecord.SimplyAutoRecordType.OtherRecord;
                    break;
            }
            target.Day = source.Date.Day;
            target.Month = source.Date.Month;
            target.Year = source.Date.Year;
            target.Odometer = source.Odometer;
            target.FillingStation = (source.FillingStation.Contains(".") ? source.FillingStation.Split('.')[1] : source.FillingStation + source.ServiceStationName);
            target.MissedFillUp = (source.MissedFillUp) ? 1 : 0;
            target.Notes = source.Notes;
            target.PartialTank = (source.FuelFullTank) ? 0 : 1;
            target.Qty = source.FuelLiters.GetValueOrDefault(0);
            target.TotalCost = source.TotalCost.GetValueOrDefault(0);
            target.RecordDesc = (source.OtherEvent + "," + source.ServiceDetail + "," + source.ServiceDetailMakeModel).Replace('*', ',').Trim(',');
            target.RecordDesc = target.RecordDesc.Length == 0 ? "-" : target.RecordDesc;

            return target;
        }


    }
}
