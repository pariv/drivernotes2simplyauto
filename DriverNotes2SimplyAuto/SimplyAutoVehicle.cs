﻿using CsvHelper.Configuration.Attributes;

namespace DriverNotes2SimplyAuto
{
    internal class SimplyAutoVehicle
    {
        [Name("Row ID")]
        public int RowId { get; set; }

        [Name("Make")]
        public string Make { get; set; }

        [Name("Model")]
        public string Model { get; set; }

        [Name("Fuel Type")]
        public string FuelType { get; set; }

        [Name("Year")]
        public string Year { get; set; }

        [Name("Lic#")]
        public string LicNo { get; set; }

        [Name("VIN")]
        public string VIN { get; set; }

        [Name("Insurance#")]
        public string InsuranceNo { get; set; }

        [Name("Notes")]
        public string Notes { get; set; }

        [Name("Picture Path")]
        public string PicturePath { get; set; }

        [Name("Vehicle ID")]
        public string VehicleId { get; set; }

        [Name("Other Specs")]
        public string OtherSpecs { get; set; }

    }
}
